module Results
  extend self

  def parse_rails_result(result_code, result, log_file, node_name, currently_updating_node, passes, failures)
    if result_code == 200
      passes += 1
      File.write(log_file, "#{node_name} - Success\n", mode: "a")
    else
      allowed_to_fail = node_name == currently_updating_node
      File.write(log_file, "#{node_name} - #{result_code} - #{allowed_to_fail ? 'Expected' : 'Error'}\n #{result}\n", mode: "a")
      failures = parse_result(node_name, currently_updating_node, allowed_to_fail, failures, result_code)
    end

    [passes, failures]
  end

  def parse_git_result(result, log_file, node_name, error, std_error, currently_updating_node, passes, failures)
    if result.success?
      passes += 1
      File.write(log_file, "#{node_name} - Success\n", mode: "a")
    else
      allowed_to_fail = node_name == currently_updating_node || (currently_updating_node.include? "-gitaly-")
      File.write(log_file, "#{node_name} - #{error} - #{allowed_to_fail ? 'Expected' : 'Error'}\n #{std_error}\n", mode: "a")
      failures = parse_result(node_name, currently_updating_node, allowed_to_fail, failures, error)
    end

    [passes, failures]
  end

  def parse_result(node_name, currently_updating_node, allowed_to_fail, failures, result)
    updated = false

    failures.collect do |f|
      next unless f.failing_node == node_name

      if (f.error_level == 'warn' && allowed_to_fail || f.error_level == 'error' && !allowed_to_fail) && f.error == result
        f.count += 1
        updated = true
      end
    end
    failures << TestFailure.new(node_name, result, allowed_to_fail ? 'warn' : 'error', currently_updating_node, 1) unless updated
    failures
  end
end
