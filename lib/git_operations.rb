module GitOperations
  extend self

  require 'open3'
  require_relative 'results'
  require_relative 'test_failure'

  @total_requests = 0
  @passes = 0
  @failures = []

  def total_requests
    @total_requests
  end

  def passes
    @passes
  end

  def failures
    @failures
  end

  def perform_git_operations(log_dir, run_file, tmp_dir, rails_nodes, delay)
    log_file = "#{log_dir}/git.log"
    threads = []
    i = 0

    File.write(log_file, "Start Git Operations")

    while File.exist?(run_file)
      i += 1
      currently_updating_node = File.exist?("#{tmp_dir}/zero-downtime-current") ? File.open("#{tmp_dir}/zero-downtime-current").map(&:chomp)[0] : 'None'
      File.write(log_file, "\nStart - Loop: #{i} - #{Time.now.getutc} - #{currently_updating_node}\n", mode: "a")

      rails_nodes.each do |node|
        threads << Thread.new do
          @total_requests += 1
          clone_dir = "#{tmp_dir}/zdt/#{node['name']}"
          repo_url = node['ip'] + (node['ssl_port'].nil? ? '' : ":#{node['ssl_port']}")

          _, clone_err, clone_status = Open3.capture3("git clone ssh://git@#{repo_url}/#{node['test_repo']} #{clone_dir}")

          if clone_status.success?
            Open3.capture3("echo '#{i}' >> #{clone_dir}/#{node['name']}.md")
            Open3.capture3("(cd #{clone_dir} && git add .)")
            Open3.capture3("(cd #{clone_dir} && git commit -m'#{node['name']} #{i}')")

            _, push_err, push_status = Open3.capture3("(cd #{clone_dir} && git push)")
            @passes, @failures = Results.parse_git_result(push_status, log_file, node['name'], 'Push Failed', push_err, currently_updating_node, @passes, @failures)
          else
            @passes, @failures = Results.parse_git_result(clone_status, log_file, node['name'], 'Clone Failed', clone_err, currently_updating_node, @passes, @failures)
          end

          Open3.capture3("rm -rf #{clone_dir}")
        end
      end

      threads.each(&:join)
      sleep delay
      File.write(log_file, "End - #{Time.now.getutc}\n", mode: "a")
    end
  end
end
